-- Main file, calls the other files and triggers main functions

mapgen_rivers = {}

local modpath = minetest.get_modpath(minetest.get_current_modname()) .. '/'
mapgen_rivers.modpath = modpath
mapgen_rivers.world_data_path = minetest.get_worldpath() .. '/river_data/'

dofile(modpath .. 'settings.lua')
dofile(modpath .. 'gridmanager.lua')
dofile(modpath .. 'polygons.lua')
dofile(modpath .. 'heightmap.lua')
dofile(modpath .. 'mapgen.lua')
dofile(modpath .. 'spawn.lua')

minetest.register_on_mods_loaded(mapgen_rivers.load_or_generate_grid)
