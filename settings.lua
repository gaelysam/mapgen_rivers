-- Read global and per-world settings

local mtsettings = minetest.settings
local mgrsettings = Settings(minetest.get_worldpath() .. '/mapgen_rivers.conf')

mapgen_rivers.version = "1.0.2-dev1"

local previous_version_mt = mtsettings:get("mapgen_rivers_version") or "0.0"
local previous_version_mgr = mgrsettings:get("version") or "0.0"

if mapgen_rivers.version ~= previous_version_mt or mapgen_rivers.version ~= previous_version_mgr then
	local compat_mt, compat_mgr = dofile(minetest.get_modpath(minetest.get_current_modname()) .. "/compatibility.lua")
	if mapgen_rivers.version ~= previous_version_mt then
		compat_mt(mtsettings)
	end
	if mapgen_rivers.version ~= previous_version_mgr then
		compat_mgr(mgrsettings)
	end
end

mtsettings:set("mapgen_rivers_version", mapgen_rivers.version)
mgrsettings:set("version", mapgen_rivers.version)

local defaults
do
	local f = io.open(mapgen_rivers.modpath .. "/settings_default.json")
	defaults = minetest.parse_json(f:read("*all"))
	f:close()
end

-- Convert strings to numbers in noise params because Minetest API is not able to do it cleanly...
local function clean_np(np)
	for field, value in pairs(np) do
		if field ~= 'flags' and type(value) == 'string' then
			np[field] = tonumber(value) or value
		elseif field == 'spread' then
			for dir, v in pairs(value) do
				value[dir] = tonumber(v) or v
			end
		end
	end
end

function mapgen_rivers.define_setting(name, dtype, default)
	if dtype == "number" or dtype == "string" then
		local v = mgrsettings:get(name)
		if v == nil then
			v = mtsettings:get('mapgen_rivers_' .. name)
			if v == nil then
				v = defaults[name]
			end
			mgrsettings:set(name, v)
		end
		if dtype == "number" then
			return tonumber(v)
		else
			return v
		end
	elseif dtype == "bool" then
		local v = mgrsettings:get_bool(name)
		if v == nil then
			v = mtsettings:get_bool('mapgen_rivers_' .. name)
			if v == nil then
				v = defaults[name]
			end
			mgrsettings:set_bool(name, v)
		end
		return v
	elseif dtype == "noise" then
		local v = mgrsettings:get_np_group(name)
		if v == nil then
			v = mtsettings:get_np_group('mapgen_rivers_' .. name)
			if v == nil then
				v = defaults[name]
			end
			mgrsettings:set_np_group(name, v)
		end
		clean_np(v)
		return v
	end
end

local def_setting = mapgen_rivers.define_setting

mapgen_rivers.settings = {
	center = def_setting('center', 'bool'),
	blocksize = def_setting('blocksize', 'number'),
	sea_level = tonumber(minetest.get_mapgen_setting('water_level')),
	min_catchment = def_setting('min_catchment', 'number'),
	river_widening_power = def_setting('river_widening_power', 'number'),
	riverbed_slope = def_setting('riverbed_slope', 'number'),
	distort = def_setting('distort', 'bool'),
	biomes = def_setting('biomes', 'bool'),
	glaciers = def_setting('glaciers', 'bool'),
	glacier_factor = def_setting('glacier_factor', 'number'),
	elevation_chill = def_setting('elevation_chill', 'number'),

	map_x_size = def_setting('map_x_size', 'number'),
	map_z_size = def_setting('map_z_size', 'number'),
	margin = def_setting('margin', 'bool'),
	margin_width = def_setting('margin_width', 'number'),
	margin_elev = def_setting('margin_elev', 'number'),
	evol_params = {
		K = def_setting('river_erosion_coef', 'number'),
		m = def_setting('river_erosion_power', 'number'),
		d = def_setting('diffusive_erosion', 'number'),
		compensation_radius = def_setting('compensation_radius', 'number'),
	},
	tectonic_speed = def_setting('tectonic_speed', 'number'),
	evol_time = def_setting('evol_time', 'number'),
	evol_time_step = def_setting('evol_time_step', 'number'),
}

mapgen_rivers.noise_params = {
	base = def_setting("np_base", "noise"),
	distort_x = def_setting("np_distort_x", "noise"),
	distort_z = def_setting("np_distort_z", "noise"),
	distort_amplitude = def_setting("np_distort_amplitude", "noise"),

	heat = minetest.get_mapgen_setting_noiseparams('mg_biome_np_heat'),
	heat_blend = minetest.get_mapgen_setting_noiseparams('mg_biome_np_heat_blend'),
}

mapgen_rivers.noise_params.heat.offset = mapgen_rivers.noise_params.heat.offset +
		mapgen_rivers.settings.sea_level * mapgen_rivers.settings.elevation_chill

local function write_settings()
	mgrsettings:write()
end

minetest.register_on_mods_loaded(write_settings)
minetest.register_on_shutdown(write_settings)
